# LaTeX für Seminararbeiten - in 90 Minuten 
or LaTeX for Term Papers - in 90 minutes (hence ltxtp90)

[Präsentation als PDF](tex/slides/ltxtp90_slides.pdf)

## Warum?
Weil es manchmal praktisch ist Studenten (oder sich selbst) ein schnelle 
Einführung in die Grundlagen von LaTeX zu geben. Es geht dabei vor allem darum 
innerhalb von z. B. einem Veranstaltungsblock an der Uni genug Wissen zu 
vermitteln um z. B. eine Seminararbeit einheitlich und strukturiert setzen zu 
können (die Dozentin / den Dozenten wird es freuen ;) ). Gleichzeitig soll die 
Hemmschwelle niedrig gehalten und vermittelt werden, dass LaTeX - trotz 
all den automagischen Funktionen - keine Zauberei ist und sogar Spaß machen 
kann. Natürlich ist so eine Einführung zwangsläufig unvollständig, deshalb 
sollen Hinweise und Links die vielen Möglichkeiten zeigen sich weitere 
Funktionen selbstständig zu erarbeiten.

Für sachdienliche Hinweise zur Verbesserung bin ich immer dankbar. Allerdings 
möchte ich den Kurs nicht verlängern und vor allem die LaTeX-Funktionen 
vorstellen die in meinem Lehrkontext wichtig sind. Natürlich sind die 
Geschmäcker verschieden, weshalb ich dazu aufrufe im Zweifel einfach einen 
Fork anzulegen und zur Vielefalt in der Lehre beizutragen ;)

## Vorbereitung
Um den Kurs vorzubreiten empfiehlt es sich vorab dafür zu sorgen, dass eine 
funktionierende LaTeX-Version auf dem jeweiligen Rechner installiert ist. Gerade
in Präsenzveranstaltungen mit vielen Teilnehmern kann es sonst sehr viel Zeit in 
Anspruch nehmen erst einmal alles lauffähig zu kriegen.

Perönlich verwende ich gerne eine Kombination aus Texmaker und JabRef zusammen 
mit MiKTeX (unter Windows) und TeXLive (unter Linux).

## Lizenz
![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)

"LaTeX für Seminararbeiten - in 90 Minuten" von Arne Bethmann ist lizenziert unter einer [Creative Commons Namensnennung 4.0 International Lizenz](http://creativecommons.org/licenses/by/4.0/).